﻿using NIM;
using NIM.Session;
using NimUtility.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class main : MonoBehaviour {

    string acc = "";
    string token = "";


    public InputField inputAcc;
    public InputField inputToken;
    public InputField appKey;



    public GameObject gameui;
    public GameObject loginui;

    public Text tip;
    public Text txtChat;

    public InputField input;
    public InputField inputChat;
    public InputField inputChatTarget;
    public InputField inputChatTeamTarget;


    public InputField inputTeam;

    // Use this for initialization
    void Start () {
        //var conf = new NimUtility.NimConfig();
        var ok = ClientAPI.Init(main.AppDataPath);
        if (!ok)
        {
            Debug.LogError("init fail");
            return;
        }

        //监听发送事件
        NIM.TalkAPI.OnSendMessageCompleted += OnSendMessageResult;
        //取消监听
        //NIM.TalkAPI.OnSendMessageCompleted -= OnSendMessageResult;

        //监听接受事件
        NIM.TalkAPI.OnReceiveMessageHandler += OnMessageReceived;

        //取消监听
        //NIM.TalkAPI.OnReceiveMessageHandler -= OnMessageReceived;


        //注册全局的消息状态变更通知
        NIM.Messagelog.MessagelogAPI.RegMsglogStatusChangedCb(MsglogStatusChangedDelegate);

    }

 
    // Update is called once per frame
    void Update () {
        refreshChat();
    }

    void OnDestroy()
    {
        Debug.Log("destroy");
        ClientAPI.Logout(NIM.NIMLogoutType.kNIMLogoutAppExit, 1);
        ClientAPI.Cleanup();
    }


    public void DoLogin()
    {
        acc = inputAcc.text;
        token = inputToken.text;
        Debug.Log("login..." + acc + " " + token);
        var key = appKey.text;
        ClientAPI.Login(key, acc, token, HandleLoginResult);
        gameui.SetActive(true);
        loginui.SetActive(false);
    }

    private void HandleLoginResult(NIM.NIMLoginResult result)
    {
        Debug.Log("HandleLoginResult:" + result.LoginStep +" code:"+result.Code);
        //处理登录结果
        switch (result.LoginStep)
        {
            case NIMLoginStep.kNIMLoginStepLinking:
                //建立连接
                break;
            case NIMLoginStep.kNIMLoginStepLink:
                //连接到服务器
                break;
            case NIMLoginStep.kNIMLoginStepLogin:
                //登录验证
                if (result.Code == NIM.ResponseCode.kNIMResSuccess)
                {
                    //登录成功
                    Debug.Log("login success!");
                }
                else
                {
                    //登录失败，根据result.Code检查登录失败原因
                    Debug.Log("login fail:"+result.Code);
                }

                break;
        }
    }

    static string _appDataPath;
    public static string AppDataPath
    {
        get
        {
            if (string.IsNullOrEmpty(_appDataPath))
            {
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    _appDataPath = Application.persistentDataPath + "/NimUnityDemo";
                }
                else if (Application.platform == RuntimePlatform.Android)
                {
                    string androidPathName = "com.netease.nim_unity_android_demo";
                    if (System.IO.Directory.Exists("/sdcard"))
                        _appDataPath = System.IO.Path.Combine("/sdcard", androidPathName);
                    else
                        _appDataPath = System.IO.Path.Combine(Application.persistentDataPath, androidPathName);
                }
                else if (Application.platform == RuntimePlatform.WindowsEditor ||
                Application.platform == RuntimePlatform.WindowsPlayer)
                {
                    _appDataPath = "NimUnityDemo";
                }
                Debug.Log("AppDataPath:" + _appDataPath);
            }
            return _appDataPath;
        }
    }



    public void CmdAddFriend()
    {
        if (input.text.Length<1)
        {
            Debug.LogWarning("input target!");
            return;
        }
        var target = input.text;
        //直接添加好友，不需要对方验证
        NIM.Friend.FriendAPI.ProcessFriendRequest(target, NIM.Friend.NIMVerifyType.kNIMVerifyTypeAdd, "", (int resCode, string jsonExtension, IntPtr userData)=>
        {
            NIM.ResponseCode code = (ResponseCode)resCode;
            //处理操作结果，如果使用需要对方验证的方式添加好友，此处不能表明添加好友成功，需要等待对方验证
            Debug.Log("addfriend result:" + code);
        });
        //添加好友，需要对方验证
        //NIM.Friend.FriendAPI.ProcessFriendRequest("FRIEND ID", NIMVerifyType.kNIMVerifyTypeAsk, "加好友，请求验证通过", OnAddFriend);
    }

    #region team
    public void CmdGetAllTeam()
    {
        NIM.Team.TeamAPI.QueryAllMyTeams((count, countIdList)=>{
            Debug.Log("team count:" + count);
            //count:查询到的群id总数，countIdList： 群id数组
            if (countIdList != null)
            {
                foreach(var t in countIdList)
                {
                    Debug.Log(t);
                    NIM.Team.TeamAPI.QueryTeamMembersInfo(t, true, false,(tid, memberCount, includeUserInfo, infoList)=>{
                        //infoList:查询返回的群成员信息列表，includeUserInfo为false时，infoList仅查询群成员id
                        Debug.Log("members:" + memberCount+" tid:"+tid);
                        foreach(var m in infoList)
                        {
                            Debug.Log("m:" + m.AccountId + "  " + m.NickName);
                        }
                    });
    }
            }
        });
    }

    public void CmdCreateTeam()
    {
        Debug.Log("CmdCreateTeam:" + inputTeam.text+ inputTeam.text.Length);
        if (inputTeam.text.Length < 1)
        {
            Debug.LogWarning("input teamname!");
            return;
        }

       //以测试账号test1,test2为例
       var ti = new NIM.Team.NIMTeamInfo
        {
            Name = inputTeam.text,
            Announcement = "群公告",
            Introduce = "群简介",
            TeamType = NIM.Team.NIMTeamType.kNIMTeamTypeNormal
       };
        string[] ids = new string[] { "test01", "test02" };
        NIM.Team.TeamAPI.CreateTeam(ti, ids, "附言信息", (data) => {
            Debug.Log("create team result:" + data.NotificationId +" teamid:"+ data.TeamEvent.TeamId);
        });
    }



    #endregion


    #region friend
    public void DelFriend()
    {
        if (input.text.Length < 1)
        {
            Debug.LogWarning("input target!");
            return;
        }
        var target = input.text;
        NIM.Friend.FriendAPI.DeleteFriend(target, (int resCode, string jsonExtension, IntPtr userData)=> 
        {
            UnityEngine.Debug.Log("Delete friend result:"+(ResponseCode)resCode);
        });
    
    }

    public void CmdGetFriends()
    {
        NIM.Friend.FriendAPI.GetFriendsList(result =>
        {
            if (result != null)
            {
                var s = string.Format("GetFriendsList completed,total count:{0}", result.ProfileList == null ? 0 : result.ProfileList.Length);
                Debug.Log(s);
                foreach(var r in result.ProfileList)
                {
                    Debug.Log("friend:" + r.AccountId);
                }
                //tip.text = s;
            }
        });
    }
    #endregion

    #region chat
    public void SendTeam()
    {
        if (inputChatTeamTarget.text.Length < 1)
        {
            Debug.LogWarning("input target!");
            return;
        }
        NIMTextMessage msg = new NIMTextMessage();
        msg.ReceiverID = inputChatTeamTarget.text;
        msg.SessionType = NIMSessionType.kNIMSessionTypeTeam;
        msg.TimeStamp = System.DateTime.UtcNow.Ticks;//1520500638234;//当前UNIX时间戳 毫秒
        msg.ClientMsgID = "uuid:" + UnityEngine.Random.value * 1000000;//生成自己的唯一uuid;
        msg.TextContent = inputChat.text;

        //其他消息设置,其他消息类似
        msg.Roaming = false;
        msg.SavedOffline = true;
        msg.ServerSaveHistory = true;

        Debug.Log("sendto team:" + msg.ReceiverID);
        TalkAPI.SendMessage(msg, ReportUploadProgressDelegate);
    }

    public void SendChatP2P()
    {
        if (inputChatTarget.text.Length < 1)
        {
            Debug.LogWarning("input target!");
            return;
        }
        //以点对消息为例，测试账号test1
        NIMTextMessage msg = new NIMTextMessage();
        msg.ReceiverID = inputChatTarget.text;
        msg.SessionType = NIMSessionType.kNIMSessionTypeP2P;
        msg.TimeStamp = System.DateTime.UtcNow.Ticks;//1520500638234;//当前UNIX时间戳 毫秒
        msg.ClientMsgID = "uuid:"+ UnityEngine.Random.value*1000000;//生成自己的唯一uuid;
        msg.TextContent = inputChat.text;
        
        //其他消息设置,其他消息类似
        msg.Roaming = false;
        msg.SavedOffline = true;
        msg.ServerSaveHistory = true;

        
        if (false)
        {
            //如果需要反垃圾
            msg.AntiSpamEnabled = true;
            //组装反垃圾内容
            JsonExtension jx = new JsonExtension();
            jx.AddItem("type", 1);
            jx.AddItem("data", msg.TextContent);//如果是图片视频类型，填附件的url

            msg.AntiSpamContent = jx.Value;

        }
        Debug.Log("sendto:" +msg.ReceiverID);
        TalkAPI.SendMessage(msg, ReportUploadProgressDelegate);
    }
    void ReportUploadProgressDelegate(long uploadedSize, long totalSize, object progressData)
    {

    }

    void OnSendMessageResult(object sender, MessageArcEventArgs args)
    {
        Debug.Log("OnSendMessageResult:" + args.ArcInfo.JSON + "  " + args.ArcInfo.Response);
    }



    List<string> chatmsgs = new List<string>();
    
    void OnMessageReceived(object sender, NIMReceiveMessageEventArgs args)
    {
        Debug.Log("recv chat:" + args.Message.MessageContent.MessageType + "  " + args.Message.MessageContent.JSON);

        if (args != null && args.Message != null)
        {
            //处理各种类型的消息
            if (args.Message.MessageContent.MessageType == NIMMessageType.kNIMMessageTypeText)
            {
                lock (chatmsgs)
                {
                    var msg = args.Message.MessageContent as NIMTextMessage;
                    chatmsgs.Add(msg.TimeStamp + " "+msg.SenderNickname + ":" + msg.TextContent);
                }
            }
            else if (args.Message.MessageContent.MessageType == NIMMessageType.kNIMMessageTypeAudio)
            {
        
            }
            else if (args.Message.MessageContent.MessageType == NIMMessageType.kNIMMessageTypeVideo)
            {
      
            }
            else if (args.Message.MessageContent.MessageType == NIMMessageType.kNIMMessageTypeImage)
            {
         
            }
            else
            {
         
            }

            //异步线程不能直接操作unity对象
            
        }
  
    }

    int lastChatLen = 0;
    void refreshChat()
    {
        lock(chatmsgs)
        {
            if(chatmsgs.Count!=lastChatLen)
            {
                lastChatLen = chatmsgs.Count;
                Debug.Log("refreshChat");
                txtChat.text = "";
                chatmsgs.ForEach(o => txtChat.text += ("\n" + o));
            }
        }
        
    }
    void MsglogStatusChangedDelegate(ResponseCode res, string result)
    {
        Debug.Log("MsglogStatusChangedDelegate:"+res+" "+result);
    }
    #endregion
}
