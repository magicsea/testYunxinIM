# testYunxinIM

#### 介绍

网易云信IM unity测试DEMO,imsdk在游戏中的应用的测试。

[SDK地址](http://dev.yunxin.163.com/docs/product/IM%E5%8D%B3%E6%97%B6%E9%80%9A%E8%AE%AF/%E6%96%B0%E6%89%8B%E6%8E%A5%E5%85%A5%E6%8C%87%E5%8D%97)

#### demo功能

1. 好友
2. 群组
3. 聊天

#### 说明

1. 需要自己去官网申请，添加账号
2. 我的账号新建了test01,test02,test03用于测试
3. 聊天自己发送不会接受到，要看到效果需要两个账号测试

#### 
